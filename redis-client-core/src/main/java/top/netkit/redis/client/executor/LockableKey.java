package top.netkit.redis.client.executor;

import java.time.Duration;

/**
 * lockable key
 * @author shixinke
 */
public interface LockableKey extends CacheableKey {

    /**
     * get wait time
     * @return long
     */
    Duration getWaitTime();

    /**
     * get lock lease time
     * @return long
     */
    Duration getLeaseTime();

}
