package top.netkit.redis.client.command;

import java.util.List;
import java.util.Map;

/**
 * redis sorted set command executor
 * @author shixinke
 */
public interface SortedSetCommandExecutor {

    /**
     * add value to the set
     * @param key redis key
     * @param member member value
     * @param score member score
     * @return boolean
     */
    <V> boolean zAdd(String key, V member, Double score);

    /**
     * insert members into the set
     * @param key redis key
     * @param members members
     * @return boolean
     */
    <V> boolean zAdd(String key, Map<V, Double> members);

    /**
     * get size of the set
     * @param key redis key
     * @return int
     */
    int zCard(String key);

    /**
     * get the count by range
     * @param key redis key
     * @param min min
     * @param max max
     * @return int
     */
    int zCount(String key, double min, double max);

    /**
     * increase by step
     * @param key redis key
     * @param member member
     * @param step step
     * @return double
     */
    <V> double zIncrBy(String key, V member, double step);

    /**
     * get the inter set and store the result
     * @param storeKey stored key
     * @param sourceKey source key
     * @param destKeys destination key
     * @return boolean
     */
    boolean zInterStore(String storeKey, String sourceKey, List<String> destKeys);

    /**
     * get number by the range
     * @param key redis key
     * @param minMember min member
     * @param maxMember max member
     * @return long
     */
    long zLexCount(String key, String minMember, String maxMember);

    /**
     * get members
     * @param key redis key
     * @param start offset start
     * @param end offset end
     * @param clazz value type
     * @return Map
     */
    <V> List<Map<V, Double>> zRange(String key, int start, int end, Class<V> clazz);

    /**
     * get members without score
     * @param key redis key
     * @param start offset start
     * @param end offset end
     * @param clazz value type
     * @return Map
     */
    <V> List<V> zRangeWithoutScore(String key, int start, int end, Class<V> clazz);


    /**
     * get members by  score
     * @param key redis key
     * @param start offset start
     * @param end offset end
     * @param withScores with scores
     * @param offset offset
     * @param count count
     * @param clazz value type
     * @return Map
     */
    <V> Map<V, Double> zRangeByScore(String key, int start, int end, boolean withScores, long offset, long count, Class<V> clazz);

    /**
     * get members by reverse
     * @param key redis key
     * @param start offset start
     * @param end offset end
     * @param clazz value type
     * @return Map
     */
    <V> List<Map<V, Double>> zRevRange(String key, int start, int end, Class<V> clazz);

    /**
     * get members by score
     * @param key redis key
     * @param start offset start
     * @param end offset end
     * @param clazz value type
     * @return Map
     */
    <V> List<V> zRevRangeWithoutScore(String key, int start, int end, Class<V> clazz);

    /**
     * get members by reverse score
     * @param key redis key
     * @param start offset start
     * @param end offset end
     * @param withScores with scores
     * @param offset offset
     * @param count count
     * @param clazz value type
     * @return Map
     */
    <V> Map<V, Double> zRevRangeByScore(String key, int start, int end, boolean withScores, long offset, long count, Class<V> clazz);
    /**
     * get the rank of the member
     * @param key redis key
     * @param member member
     * @return int
     */
    <V> int zRank(String key, V member);

    /**
     * get the reverse rank of the member
     * @param key redis key
     * @param member member
     * @return int
     */
    <V> int zRevRank(String key, V member);

    /**
     * delete member
     * @param key redis key
     * @param member member
     * @return boolean
     */
    <V> boolean zRem(String key, V member);

    /**
     * multi delete members
     * @param key redis key
     * @param members members
     * @return boolean
     */
    <V> boolean zRem(String key, List<V> members);

    /**
     * delete by rank range
     * @param key redis key
     * @param start rank start
     * @param end rank end
     * @return boolean
     */
    boolean zRemRangeByRank(String key, int start, int end);

    /**
     * delete the members by score range
     * @param key redis key
     * @param min min score
     * @param max max score
     * @return boolean
     */
    boolean zRemRangeByScope(String key, double min, double max);

    /**
     * get the score of the member
     * @param key redis key
     * @param member member
     * @return Double
     */
    <V> Double zScore(String key, V member);
    /**
     * get the union of the set and store the result
     * @param storeKey stored key
     * @param sourceKey source key
     * @param destKeys destination key
     * @return boolean
     */
    boolean zUnionStore(String storeKey, String sourceKey, List<String> destKeys);
}
