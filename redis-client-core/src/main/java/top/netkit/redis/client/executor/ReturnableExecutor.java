package top.netkit.redis.client.executor;

/**
 * executor with return value
 * @author shixinke
 */
public interface ReturnableExecutor<R, T> {

    /**
     * business execute
     * @param param parameter
     * @return T return value
     */
    T execute(R param);
}
