package top.netkit.redis.client.executor;

import org.redisson.RedissonMultiLock;
import org.redisson.api.RLock;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * redis lock
 *
 * @author shixinke
 */
public interface RedisLockExecutor {

    /**
     * normal lock
     * @param key lock key
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     * @return boolean
     */
    <T> T lock(String key, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor);

    /**
     * normal lock without result
     * @param key lock key
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     */
    void lock(String key, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor);

    /**
     * normal lock
     * @param key lock key
     * @param fair is fair lock
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     * @return boolean
     */
    <T> T lock(String key, boolean fair, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor);

    /**
     * normal lock without result
     * @param key lock key
     * @param fair is fair lock
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     */
    void lock(String key, boolean fair, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor);

    /**
     * normal lock
     * @param key lock key
     * @param leaseTime lock lease time
     * @param executor business executor
     * @return boolean
     */
    <T> T lock(String key, long leaseTime, Function<RLock, T> executor);

    /**
     * normal lock without result
     * @param key lock key
     * @param leaseTime lock lease time
     * @param executor business executor
     */
    void lock(String key, long leaseTime, Consumer<RLock> executor);

    /**
     * normal lock
     * @param key lock key
     * @param waitTime lock wait time
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     * @return boolean
     */
    <T> T tryLock(String key,  long waitTime, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor);

    /**
     * normal lock without result
     * @param key lock key
     * @param waitTime wait time
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     */
    void tryLock(String key, long waitTime, long leaseTime,  TimeUnit timeUnit, Consumer<RLock> executor);

    /**
     * normal lock
     * @param key lock key
     * @param fair 是否公平锁
     * @param waitTime lock wait time
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     * @return boolean
     */
    <T> T tryLock(String key,  boolean fair, long waitTime, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor);

    /**
     * normal lock without result
     * @param key lock key
     * @param fair 是否公平锁
     * @param waitTime wait time
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     */
    void tryLock(String key, boolean fair, long waitTime, long leaseTime,  TimeUnit timeUnit, Consumer<RLock> executor);




    /**
     * normal lock
     * @param key lock key
     * @param waitTime wait time
     * @param leaseTime lock lease time
     * @param executor business executor
     * @return boolean
     */
    <T> T tryLock(String key, long waitTime, long leaseTime, Function<RLock, T> executor);

    /**
     * normal lock without result
     * @param key lock key
     * @param waitTime lock wait time
     * @param leaseTime lock lease time
     * @param executor business executor
     */
    void tryLock(String key, long waitTime, long leaseTime, Consumer<RLock> executor);

    /**
     * get read lock
     * @param key lock key
     * @param readWaitTime read lock wait time
     * @param readLeaseTime read lock lease time
     * @param writeWaitTime write lock wait time
     * @param writeLeaseTime write lock lease time
     * @param readExecutor business executor when get read lock
     * @param writeExecutor business executor when get write lock
     * @return T
     */
    <R, T> T readWriteLock(String key, long readWaitTime, long readLeaseTime,  long writeWaitTime, long writeLeaseTime, Function<ReadWriteLock, R> readExecutor, BiFunction<ReadWriteLock, R, T> writeExecutor);


    /**
     * multi lock
     * @param keys lock keys
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     * @return T
     */
    <T> T multiLock(List<String> keys, long leaseTime, TimeUnit timeUnit, Function<RedissonMultiLock, T> executor);

    /**
     * multi lock
     * @param keys lock keys
     * @param leaseTime lock lease time
     * @param executor business executor
     * @return T
     */
    <T> T multiLock(List<String> keys, long leaseTime, Function<RedissonMultiLock, T> executor);



    /**
     * multi lock
     * @param keys lock keys
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     */
    void multiLock(List<String> keys, long leaseTime, TimeUnit timeUnit, Consumer<RedissonMultiLock> executor);

    /**
     * multi lock
     * @param keys lock keys
     * @param leaseTime lock lease time
     * @param executor business executor
     */
    void multiLock(List<String> keys, long leaseTime, Consumer<RedissonMultiLock> executor);



    /**
     * multi lock
     * @param keys lock keys
     * @param waitTime lock wait time
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     * @return T
     */
    <T> T tryMultiLock(List<String> keys, long waitTime, long leaseTime, TimeUnit timeUnit, Function<RedissonMultiLock, T> executor);

    /**
     * multi lock
     * @param keys lock keys
     * @param waitTime lock wait time
     * @param leaseTime lock lease time
     * @param executor business executor
     * @return T
     */
    <T> T tryMultiLock(List<String> keys, long waitTime, long leaseTime, Function<RedissonMultiLock, T> executor);



    /**
     * multi lock
     * @param keys lock keys
     * @param waitTime lock wait time
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     */
    void tryMultiLock(List<String> keys, long waitTime, long leaseTime, TimeUnit timeUnit, Consumer<RedissonMultiLock> executor);

    /**
     * multi lock
     * @param keys lock keys
     * @param waitTime lock wait time
     * @param leaseTime lock lease time
     * @param executor business executor
     */
    void tryMultiLock(List<String> keys, long waitTime, long leaseTime, Consumer<RedissonMultiLock> executor);

}
