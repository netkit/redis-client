package top.netkit.redis.client.executor;

import java.time.Duration;

/**
 * 可过期的key
 * @author shixinke
 */
public interface ExpireAbleKey extends CacheableKey {

    /**
     * get expire time
     * @return long
     */
    Duration getExpireTime();





}
