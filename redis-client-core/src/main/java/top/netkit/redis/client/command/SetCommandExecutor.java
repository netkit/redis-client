package top.netkit.redis.client.command;

import java.util.List;
import java.util.Set;

/**
 * set command executor
 * @author shixinke
 */
public interface SetCommandExecutor {

    /**
     * add element to the set
     * @param key key of the set
     * @param value value
     * @return boolean
     */
    <V> boolean sAdd(String key, V value);

    /**
     * add elements to the set
     * @param key set key
     * @param values values
     * @return boolean
     */
    <V> boolean sAdd(String key, List<V> values);

    /**
     * get the count of the set
     * @param key set key
     * @return int
     */
    int sCard(String key);

    /**
     * get the diff of the source set and destination set
     * @param sourceKey source key
     * @param destKey destination key
     * @param clazz value type
     * @return boolean
     */
    <V> Set<V> sDiff(String sourceKey, String destKey, Class<V> clazz);

    /**
     * get the diff of the source set and destination sets
     * @param sourceKey source key
     * @param destKeys destination keys
     * @param clazz value type
     * @return boolean
     */
    <V> Set<V> sDiff(String sourceKey, List<String> destKeys, Class<V> clazz);

    /**
     * get the diff of the source set and destination set and store the result
     * @param storeKey store key
     * @param sourceKey source key
     * @param destKeys destination keys
     * @param clazz value type
     * @return boolean
     */
    <V> boolean sDiffStore(String storeKey, String sourceKey, List<String> destKeys, Class<V> clazz);

    /**
     * get the inter of the source set and destination set
     * @param sourceKey source key
     * @param destKey destination key
     * @param clazz value type
     * @return boolean
     */
    <V> Set<V> sInter(String sourceKey, String destKey, Class<V> clazz);

    /**
     * get the inter of the source set and destination set
     * @param sourceKey source key
     * @param destKeys destination keys
     * @param clazz value type
     * @return boolean
     */
    <V> Set<V> sInter(String sourceKey, List<String> destKeys, Class<V> clazz);

    /**
     * get the inter of the source set and destination set and store the result
     * @param storeKey stored key
     * @param sourceKey source key
     * @param destKeys destination keys
     * @param clazz value type
     * @return boolean
     */
    <V> boolean sInterStore(String storeKey, String sourceKey, List<String> destKeys, Class<V> clazz);

    /**
     * get the union of the source set and destination set
     * @param sourceKey source key
     * @param destKey destination key
     * @param <V> value type
     * @param clazz value type
     * @return Set
     */
    <V> Set<V> sUnion(String sourceKey, String destKey, Class<V> clazz);

    /**
     * get the union of the source set and destination set
     * @param sourceKey source key
     * @param destKeys destination keys
     * @param <V> value type
     * @param clazz value type
     * @return Set
     */
    <V> Set<V> sUnion(String sourceKey, List<String> destKeys, Class<V> clazz);

    /**
     * get the union of the source set and destination set and store the result
     * @param storeKey stored key
     * @param sourceKey source key
     * @param destKeys destination key
     * @param clazz value type
     * @return boolean
     */
    <V> boolean sUnionStore(String storeKey, String sourceKey, List<String> destKeys, Class<V> clazz);

    /**
     * check the key is exist
     * @param key set key
     * @param member member
     * @return boolean
     */
    <V> boolean sIsMember(String key, V member);

    /**
     * get all members of the key
     * @param key key
     * @param <V> value type
     * @param clazz value type
     * @return Set
     */
    <V> Set<V> sMembers(String key, Class<V> clazz);

    /**
     * move a value to another set
     * @param sourceKey source key
     * @param destKey destination key
     * @param value moved value
     * @return boolean
     */
    <V> boolean sMove(String sourceKey, String destKey, V value);

    /**
     * delete by random
     * @param key set key
     * @param count the count of deleted member
     * @param clazz value type
     * @return Set
     */
    <V> Set<V> sPop(String key, int count, Class<V> clazz);

    /**
     *  delete member from a set
     * @param key set key
     * @param value the value will be delete
     * @return boolean
     */
    <V> boolean sRem(String key, V value);

    /**
     * delete member from the set
     * @param key set key
     * @param values the values will be delete
     * @return boolean
     */
    <V> boolean sRem(String key, List<V> values);

    /**
     * get an element by random
     * @param key key of the set
     * @param count count of the member
     * @param <V> result type
     * @param clazz result class type
     * @return Set
     */
    <V> Set<V> sRandomMember(String key, int count, Class<V> clazz);
}
