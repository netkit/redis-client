package top.netkit.redis.client.command;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;
import java.util.Map;

/**
 * redis string command executor
 * @author shixinke
 */
public interface StringCommandExecutor {
    /**
     * append the value to the key
     * @param key redis key
     * @param value value
     * @return boolean
     */
    boolean append(String key, String value);

    /**
     * count
     * @param key redis key
     * @return long
     */
    long bitCount(String key);

    /**
     * bit operation
     * @param destKey destination key
     * @param operation operation
     * @param keys keys
     */
    void bitOp(String destKey, String operation, String...keys);

    /**
     * bit and operation
     * @param destKey destination key
     * @param keys keys
     */
    void and(String destKey, String...keys);

    /**
     * bit or operation
     * @param destKey destination key
     * @param keys keys
     */
    void or(String destKey, String...keys);

    /**
     * bit not operation
     * @param destKey destination key
     */
    void not(String destKey);

    /**
     * bit xor operation
     * @param destKey destination key
     * @param keys keys
     */
    void xor(String destKey, String... keys);


    /**
     * set a value
     * @param key redis key
     * @param value value
     * @return boolean
     */
    <V> boolean set(String key, V value);

    /**
     * set value with expire
     * @param key redis key
     * @param value value
     * @param expireSeconds expire seconds
     * @return boolean
     */
    <V> boolean set(String key, V value, long expireSeconds);

    /**
     * set value and set expire(seconds)
     * @param key redis key
     * @param value value
     * @param expireSeconds expire seconds
     * @return boolean
     */
    <V> boolean setEx(String key, V value, long expireSeconds);

    /**
     * set value and set expire(mills)
     * @param key redis key
     * @param value value
     * @param expireMills expire mills
     * @return boolean
     */
    <V> boolean pSetEx(String key, V value, long expireMills);

    /**
     * set the value when the key is not exist
     * @param key redis key
     * @param value value
     * @return boolean
     */
    <V> boolean setNx(String key, V value);

    /**
     * get the value
     * @param key redis key
     * @return String
     */
    String get(String key);

    /**
     * get the value
     * @param key redis key
     * @param clazz value type
     * @param <V> value type
     * @return V
     */
    <V> V get(String key, Class<V> clazz);

    /**
     * get the value
     * @param key redis key
     * @param typeReference value type
     * @param <V> value type
     * @return V
     */
    <V> V get(String key, TypeReference<V> typeReference);


    /**
     * set the value and return the old value
     * @param key redis key
     * @param value value
     * @param clazz value type
     * @param <V> value type
     * @return V
     */
    <V> V getSet(String key, V value, Class<V> clazz);

    /**
     * increase by step
     * @param key redis key
     * @return long
     */
    long incr(String key);

    /**
     * increase by step
     * @param key redis key
     * @param step step
     * @return long
     */
    long incrBy(String key, int step);

    /**
     * increase by step
     * @param key redis key
     * @param step step
     * @return double
     */
    double incrByFloat(String key, float step);

    /**
     * decrease
     * @param key redis key
     * @return long
     */
    long decr(String key);

    /**
     * decrease by the step
     * @param key redis key
     * @param step step
     * @return long
     */
    long decrBy(String key, int step);

    /**
     * multi get values
     * @param keys redis keys
     * @param clazz value type
     * @param <V> value type
     * @return Map
     */
    <V> Map<String, V> mGet(List<String> keys, Class<V> clazz);

    /**
     * multi get
     * @param keys redis keys
     * @param typeReference value type
     * @param <V> value type
     * @return Map
     */
    <V> Map<String, V> mGet(List<String> keys, TypeReference<V> typeReference);

    /**
     * multi set values
     * @param valueMap values map
     * @return boolean
     */
    <V> boolean mSet(Map<String, V> valueMap);

    /**
     * multi set values and set expire
     * @param valueMap values map
     * @param expire expire
     * @param <V> value type
     * @return boolean
     */
    <V> boolean mSetEx(Map<String, V> valueMap, long expire);

    /**
     * multi set values when the key is not exist
     * @param valueMap values map
     * @return boolean
     */
    <V> boolean mSetNx(Map<String, V> valueMap);

    /**
     * set the value by the offset
     * @param key redis key
     * @param offset offset
     * @param value value
     * @return boolean
     */
    boolean setRange(String key, int offset, String value);

    /**
     * get the length of the value
     * @param key redis key
     * @return long
     */
    long strLen(String key);
}
