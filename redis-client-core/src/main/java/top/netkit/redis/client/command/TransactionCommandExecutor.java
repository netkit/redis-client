package top.netkit.redis.client.command;

import org.redisson.api.RBatch;
import org.redisson.api.RTransaction;
import top.netkit.redis.client.executor.ReturnableExecutor;

/**
 * redis transaction command executor
 * @author shixinke
 */
public interface TransactionCommandExecutor {

    /**
     * discard the transaction
     * @param transaction transaction
     * @return boolean
     */
    boolean discard(RTransaction transaction);

    /**
     * start and transaction
     * @param executor executor
     * @return boolean
     */
    boolean multi(ReturnableExecutor<RBatch, Boolean> executor);

    /**
     * start and transaction
     * @param executor executor
     * @return boolean
     */
    boolean pipeline(ReturnableExecutor<RBatch, Boolean> executor);
}
