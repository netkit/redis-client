package top.netkit.redis.client.command;



import java.util.List;

/**
 * script command executor
 * @author shixinke
 */
public interface ScriptCommandExecutor {

    /**
     * execute the script
     * @param key key
     * @param script script
     * @param readonly script running mode
     * @param keys keys
     * @param args arguments
     * @param clazz return type
     * @return V
     */
    <V> V eval(String key, String script, boolean readonly, List<Object> keys, List<Object> args, Class<V> clazz);

    /**
     * execute the script
     * @param script script
     * @param readonly script running mode
     * @param keys keys
     * @param args arguments
     * @param clazz return type
     * @return V
     */
    <V> V eval(String script, boolean readonly, List<Object> keys, List<Object> args, Class<V> clazz);

    /**
     * execute lua script
     * @param script lua script
     * @param readonly readonly
     * @param clazz response class type
     * @return V
     */
    <V> V eval(String script, boolean readonly, Class<V> clazz);

    /**
     * execute the script
     * @param key key
     * @param script script
     * @param readonly script running mode
     * @param keys keys
     * @param args arguments
     * @param clazz return type
     * @return V
     */
    <V> V evalSha(String key, String script, boolean readonly, List<Object> keys, List<Object> args, Class<V> clazz);

    /**
     * execute the script
     * @param script script
     * @param readonly script running mode
     * @param keys keys
     * @param args arguments
     * @param clazz return type
     * @return V
     */
    <V> V evalSha(String script, boolean readonly, List<Object> keys, List<Object> args, Class<V> clazz);

    /**
     * execute lua script
     * @param script lua script
     * @param readonly readonly
     * @param clazz response class type
     * @return V
     */
    <V> V evalSha(String script, boolean readonly, Class<V> clazz);



    /**
     * check the scripts is exist
     * @param script script
     * @return boolean
     */
    boolean scriptExists(String script);

    /**
     * check the scripts is exists
     * @param scripts scripts
     * @return List
     */
    List<Boolean> scriptExists(List<String> scripts);

    /**
     * flush the script
     * @return boolean
     */
    boolean flushScript();

    /**
     * kill the running script
     * @return boolean
     */
    boolean killScript();

    /**
     * load script
     * @param script script
     * @param <V> return value type
     * @return V
     */
    <V> V loadScript(String script);
}
