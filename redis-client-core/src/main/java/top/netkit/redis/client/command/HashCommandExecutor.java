package top.netkit.redis.client.command;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;
import java.util.Map;

/**
 * hash command executor
 * @author shixinke
 */
public interface HashCommandExecutor extends CommandExecutor {

    /**
     * delete multi fields
     * @param key redis key
     * @param fields hash members
     * @return boolean
     */
    boolean hDel(String key, List<Object> fields);

    /**
     * delete hash member
     * @param key redis key
     * @param field hash member
     * @return boolean
     */
    boolean hDel(String key, Object field);

    /**
     * check member is exists
     * @param key hash key
     * @param field hash member
     * @return boolean
     */
    boolean hExists(String key, Object field);

    /**
     * get hash member value
     * @param key hash key
     * @param field hash member
     * @param clazz Class of hash member value
     * @param <V> member value type
     * @return V
     */
    <V> V hGet(String key, Object field, Class<V> clazz);

    /**
     * get hash member value
     * @param key hash key
     * @param field hash member
     * @param typeReference value type
     * @param <V> member item value type
     * @return V
     */
    <V> V hGet(String key, Object field, TypeReference<V> typeReference);

    /**
     * get all values of the key
     * @param key hash key
     * @param <V> hash member value type
     * @param clazz hash member value type
     * @return Map
     */
    <V> Map<Object, V> hGetAll(String key, Class<V> clazz);

    /**
     * get all values of the key
     * @param key hash key
     * @param keyClass key class
     * @param valueClass value class
     * @return Map
     */
    <K, V> Map<K, V> hGetAll(String key, Class<K> keyClass, Class<V> valueClass);

    /**
     * increment by the step
     * @param key hash key
     * @param field hash member
     * @param step step
     * @return long
     */
    long hIncr(String key, Object field, int step);

    /**
     * increment by step
     * @param key hash key
     * @param field hash member
     * @param step step
     * @return double
     */
    double hIncrByFloat(String key, Object field, double step);

    /**
     * get all the keys of the key
     * @param key hash key
     * @return List
     */
    List<Object> hKeys(String key);

    /**
     * get length of the hash
     * @param key hash key
     * @return long
     */
    long hLen(String key);

    /**
     * multi get values of the members
     * @param key hash key
     * @param fields hash member
     * @param keyClass hash member value type
     * @param valueClass hash value type
     * @param <V> hash member value type
     * @return Map
     */
    <K, V> Map<K, V> hMGet(String key, List<K> fields, Class<K> keyClass, Class<V> valueClass);

    /**
     * multi set value
     * @param key hash key
     * @param valuesMap values map
     * @return boolean
     */
    <K, V> boolean hMSet(String key, Map<K, V> valuesMap);

    /**
     * multi set value
     * @param key hash key
     * @param valuesMap values map
     * @param expire expire time
     * @return boolean
     */
    <K, V> boolean hMSet(String key, Map<K, V> valuesMap, long expire);

    /**
     * set value
     * @param key hash key
     * @param field hash member
     * @param value hash member value
     * @return boolean
     */
    <V> boolean hSet(String key, Object field, V value);

    /**
     * set member value if field is not exist
     * @param key hash key
     * @param field hash member
     * @param value hash value
     * @return boolean
     */
    <V> boolean hSetNx(String key, Object field, V value);

    /**
     * get length of a member value
     * @param key hash key
     * @param field hash member
     * @return long
     */
    long hStrLen(String key, Object field);

    /**
     * get all values of a key
     * @param key hash key
     * @param clazz hash value type
     * @param <V> hash value type
     * @return V
     */
    <V> List<V> hVals(String key, Class<V> clazz);
}
