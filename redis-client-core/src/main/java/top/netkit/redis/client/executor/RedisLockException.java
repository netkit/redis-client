package top.netkit.redis.client.executor;

import java.util.Collection;

/**
 * redis lock exception
 * @author shixinke
 */
public class RedisLockException extends RuntimeException {

    /**
     * redis lock constructor
     */
    public RedisLockException() {
        super();
    }

    public RedisLockException(String key) {
        super(String.format("get lock failed, key=%s", key));
    }

    public RedisLockException(String messageFormat, Object...args) {
        super(String.format(messageFormat, args));
    }

    public RedisLockException(Throwable cause, String messageFormat, Object...args) {
        super(String.format(messageFormat, args), cause);
    }


    public RedisLockException(Collection<String> keys) {
        super(String.format("get lock failed, key=%s", keys.toString()));
    }



    /**
     * redis lock constructor
     * @param key lock key
     * @param cause exception
     */
    public RedisLockException(String key, Throwable cause) {
        super(String.format("get lock failed, key=%s", key), cause);
    }

    /**
     * redis lock exception
     * @param cause cause
     */
    public RedisLockException(Throwable cause) {
        super(cause);
    }

    /**
     * redis lock exception
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    protected RedisLockException(String message, Throwable cause,
                               boolean enableSuppression,
                               boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
