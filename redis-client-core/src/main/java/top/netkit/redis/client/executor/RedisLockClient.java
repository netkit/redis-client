package top.netkit.redis.client.executor;

import org.redisson.RedissonMultiLock;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;


import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;


/**
 * redis lock client
 *
 * @author shixinke
 */
public class RedisLockClient implements RedisLockExecutor {

    /**
     * redisson client instance
     */
    private RedissonClient redissonClient;


    private RedisLockClient() {
    }

    public RedisLockClient(RedissonClient redissonClient) {
        this.redissonClient = redissonClient;
    }

    @Override
    public <T> T lock(String key, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor) {
        return lock(key, false, leaseTime, timeUnit, executor);
    }


    @Override
    public void lock(String key, long leaseTime, Consumer<RLock> executor) {
        lock(key, false, leaseTime, TimeUnit.MILLISECONDS, executor);
    }

    @Override
    public <T> T tryLock(String key, long waitTime, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor) {
        return tryLock(key, false, waitTime, leaseTime, timeUnit, executor);
    }

    @Override
    public void tryLock(String key, long waitTime, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor) {
        tryLock(key, false, waitTime, leaseTime, timeUnit, executor);
    }

    @Override
    public <T> T tryLock(String key, boolean fair, long waitTime, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor) {
        RLock lock = null;
        try {
            lock = createTryLock(key, fair, waitTime, leaseTime, timeUnit);
            return executor.apply(lock);
        } finally {
            releaseLock(lock);
        }
    }

    @Override
    public void tryLock(String key, boolean fair, long waitTime, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor) {
        RLock lock = null;
        try {
            lock = createTryLock(key, fair, waitTime, leaseTime, timeUnit);
            executor.accept(lock);
        } finally {
            releaseLock(lock);
        }
    }

    @Override
    public <T> T tryLock(String key, long waitTime, long leaseTime, Function<RLock, T> executor) {
        return tryLock(key, waitTime, leaseTime, TimeUnit.MILLISECONDS, executor);
    }

    @Override
    public void tryLock(String key, long waitTime, long leaseTime, Consumer<RLock> executor) {
        tryLock(key, waitTime, leaseTime, TimeUnit.MILLISECONDS, executor);
    }


    /**
     * get read lock
     * @param key lock key
     * @param readWaitTime read lock wait time
     * @param readLeaseTime read lock release time
     * @param writeWaitTime write lock wait time
     * @param writeLeaseTime write lock release time
     * @param readExecutor business executor when get read lock
     * @param writeExecutor business executor when get write lock
     * @return T
     */
    @Override
    public <R, T> T readWriteLock(String key, long readWaitTime, long readLeaseTime, long writeWaitTime, long writeLeaseTime,  Function<ReadWriteLock, R> readExecutor, BiFunction<ReadWriteLock, R, T> writeExecutor) {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock(key);
        RLock readLock = readWriteLock.readLock();
        boolean locked = false;
        try {
            locked = readLock.tryLock(readWaitTime, readLeaseTime, TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            throw new RedisLockException(key, ex);
        }

        if (!locked) {
            throw new RedisLockException("get read lock failed, key=%s", key);
        }
        R readResult = null;
        try {
            readResult = readExecutor.apply(readWriteLock);
        } finally {
            releaseLock(readLock);
        }
        if (writeExecutor == null) {
            return null;
        }
        RLock writeLock = readWriteLock.writeLock();
        locked = false;
        try {
            locked = writeLock.tryLock(writeWaitTime, writeLeaseTime, TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            releaseLock(readLock);
            throw new RedisLockException(ex, "get write lock failed, key=%s", key);
        }

        if (!locked) {
            readLock.unlock();
            throw new RedisLockException("get write lock failed, key=%s", key);
        }
        try {
            return writeExecutor.apply(readWriteLock, readResult);
        } finally {
            releaseLock(writeLock);
        }
    }

    @Override
    public void lock(String key, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor) {
        lock(key, false, leaseTime, timeUnit, executor);
    }

    @Override
    public <T> T lock(String key, long leaseTime, Function<RLock, T> executor) {
        return lock(key, false, leaseTime, TimeUnit.MILLISECONDS, executor);
    }

    @Override
    public <T> T lock(String key, boolean fair, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor) {
        RLock lock = createLock(key, fair, leaseTime, timeUnit);
        try {
            return executor.apply(lock);
        } finally {
            releaseLock(lock);
        }
    }

    @Override
    public void lock(String key, boolean fair, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor) {
        RLock lock = createLock(key, fair, leaseTime, timeUnit);
        try {
            executor.accept(lock);
        } finally {
            releaseLock(lock);
        }
    }

    /**
     * multi lock
     * @param keys lock keys
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     * @return T
     */
    @Override
    public <T> T multiLock(List<String> keys, long leaseTime, TimeUnit timeUnit, Function<RedissonMultiLock, T> executor) {
        RedissonMultiLock multiLock = createMultiLock(keys, leaseTime, timeUnit);
        try {
            return executor.apply(multiLock);
        } finally {
            releaseLock(multiLock);
        }
    }

    /**
     * multi lock
     * @param keys lock keys
     * @param leaseTime lock lease time
     * @param timeUnit time unit
     * @param executor business executor
     */
    @Override
    public void multiLock(List<String> keys, long leaseTime, TimeUnit timeUnit, Consumer<RedissonMultiLock> executor) {
        RedissonMultiLock multiLock = createMultiLock(keys, leaseTime, timeUnit);
        try {
            executor.accept(multiLock);
        } finally {
            releaseLock(multiLock);
        }
    }

    /**
     * multi lock
     * @param keys lock keys
     * @param leaseTime lock lease time
     * @param executor business executor
     * @return T
     */
    @Override
    public <T> T multiLock(List<String> keys, long leaseTime, Function<RedissonMultiLock, T> executor) {
        return multiLock(keys, leaseTime, TimeUnit.MILLISECONDS, executor);
    }

    /**
     * multi lock
     * @param keys lock keys
     * @param leaseTime lock lease time
     * @param executor business executor
     */
    @Override
    public void multiLock(List<String> keys, long leaseTime, Consumer<RedissonMultiLock> executor) {
        multiLock(keys, leaseTime, TimeUnit.MILLISECONDS, executor);
    }

    @Override
    public <T> T tryMultiLock(List<String> keys, long waitTime, long leaseTime, TimeUnit timeUnit, Function<RedissonMultiLock, T> executor) {
        RedissonMultiLock multiLock = createMultiLock(keys, waitTime, leaseTime, timeUnit);
        try {
            return executor.apply(multiLock);
        } finally {
            releaseLock(multiLock);
        }
    }

    @Override
    public <T> T tryMultiLock(List<String> keys, long waitTime, long leaseTime, Function<RedissonMultiLock, T> executor) {
        return multiLock(keys, leaseTime, TimeUnit.MILLISECONDS, executor);
    }

    @Override
    public void tryMultiLock(List<String> keys, long waitTime, long leaseTime, TimeUnit timeUnit, Consumer<RedissonMultiLock> executor) {
        RedissonMultiLock multiLock = createMultiLock(keys, waitTime, leaseTime, timeUnit);
        try {
            executor.accept(multiLock);
        } finally {
            releaseLock(multiLock);
        }
    }

    @Override
    public void tryMultiLock(List<String> keys, long waitTime, long leaseTime, Consumer<RedissonMultiLock> executor) {
        multiLock(keys, leaseTime, TimeUnit.MILLISECONDS, executor);
    }

    private RLock createLock(String key, boolean fair, long leaseTime, TimeUnit timeUnit) {
        RLock lock = null;
        if (fair) {
            lock = redissonClient.getFairLock(key);
        } else {
            lock = redissonClient.getLock(key);
        }
        lock.lock(leaseTime, timeUnit);
        if (!lock.isLocked()) {
            throw new RedisLockException(key);
        }
        return lock;
    }

    private RLock createTryLock(String key, boolean fair, long waitTime, long leaseTime, TimeUnit timeUnit) {
        RLock lock = redissonClient.getLock(key);
        boolean acquired = false;
        try {
            acquired = lock.tryLock(waitTime, leaseTime, timeUnit);
        } catch (InterruptedException ex) {
            throw new RedisLockException(key, ex);
        }
        if (!acquired) {
            throw new RedisLockException(key);
        }
        return lock;
    }

    /**
     * create multi lock
     * @param keys keys
     * @param waitTime lock wait time
     * @param leaseTime lock wait time
     * @param timeUnit time unit
     * @return RedissonMultiLock
     */
    private RedissonMultiLock createMultiLock(List<String> keys, long waitTime, long leaseTime, TimeUnit timeUnit) {
        RedissonMultiLock multiLock = createMultiLock(keys);
        boolean acquired = false;
        try {
            acquired = multiLock.tryLock(waitTime, leaseTime, timeUnit);
        } catch (InterruptedException ex) {
            throw new RedisLockException(ex, String.format("get multi lock failed, keys=%s", keys));
        }
        if (!acquired) {
            throw new RedisLockException(String.format("get multi lock failed, keys=%s", keys));
        }
        return multiLock;
    }

    /**
     * create multi lock
     * @param keys keys
     * @param leaseTime lock wait time
     * @param timeUnit time unit
     * @return RedissonMultiLock
     */
    private RedissonMultiLock createMultiLock(List<String> keys, long leaseTime, TimeUnit timeUnit) {
        RedissonMultiLock multiLock = createMultiLock(keys);
        multiLock.lock(leaseTime, timeUnit);
        if (!multiLock.isLocked()) {
            throw new RedisLockException(String.format("get multi lock failed, keys=%s", keys));
        }
        return multiLock;
    }

    private RedissonMultiLock createMultiLock(List<String> keys) {
        RLock[] locks = new RLock[keys.size()];
        for (int i = 0; i< keys.size(); i++) {
            locks[i] = redissonClient.getLock(keys.get(i));
        }
        return new RedissonMultiLock(locks);
    }


    /**
     * release lock
     * @param lock lock
     */
    private void releaseLock(RLock lock) {
        if (lock == null) {
            return;
        }
        if (lock.isLocked() && lock.isHeldByCurrentThread()) {
            lock.unlockAsync();
        }
    }
}
