package top.netkit.redis.client.executor;


import org.redisson.client.codec.Codec;
import top.netkit.redis.client.command.*;

/**
 * redis command executor
 * @author shixinke
 */
public interface RedisCommandExecutor extends KeyCommandExecutor,
        StringCommandExecutor,
        HashCommandExecutor,
        ListCommandExecutor,
        SetCommandExecutor,
        SortedSetCommandExecutor,
        TransactionCommandExecutor,
        ScriptCommandExecutor,
        QueueCommandExecutor {

    /**
     * 设置编码
     * @param codec 编码
     */
    void setCodec(Codec codec);

    /**
     * 获取编码
     * @return Codec
     */
    Codec getCodec();

}
