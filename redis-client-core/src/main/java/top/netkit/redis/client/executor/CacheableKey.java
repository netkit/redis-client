package top.netkit.redis.client.executor;

/**
 * 可缓存的键
 * @author shixinke
 */
public interface CacheableKey {

    /**
     * get cache key
     * @return String
     */
    String getKey();

    /**
     * get key
     * @param args arguments
     * @return String
     */
    default String getKey(Object args) {
        return String.format(getKey(), args);
    }
}
