package top.netkit.redis.client.command;

import org.redisson.api.listener.MessageListener;
import org.redisson.api.listener.PatternMessageListener;

import java.util.List;

/**
 * queue command executor
 * @author shixinke
 */
public interface QueueCommandExecutor {

    /**
     * subscribe by the pattern of the topic
     * @param pattern topic pattern
     * @param type class Type
     * @param listener callback
     */
    <T> void pSubscribe(String pattern, Class<T> type, PatternMessageListener<T> listener);

    /**
     * subscribe multiple topic patterns
     * @param patterns topic patterns
     * @param types class type
     * @param listeners callbacks
     */
    <T> void pSubscribe(List<String> patterns, List<Class<T>> types, List<PatternMessageListener<T>> listeners);

    /**
     * unsubscribe by topic pattern
     * @param pattern topic pattern
     */
    void pUnSubscribe(String pattern);

    /**
     * unsubscribe by topic patterns
     * @param patterns topic patterns
     */
    void pUnSubscribe(List<String> patterns);

    /**
     * publish a message
     * @param channel message channel
     * @param message message content
     * @return long
     */
    <V> long publish(String channel, V message);

    /**
     * subscribe the topic
     * @param topic topic name
     * @param type class type
     * @param listener callback
     */
    <M> void subscribe(String topic, Class<M> type, MessageListener<? extends M> listener);

    /**
     * multi subscribe the topics
     * @param topics
     * @param clazz
     * @param listeners
     */
    <M> void subscribe(List<String> topics, List<Class<M>> clazz, List<MessageListener<? extends M>> listeners);

    /**
     * unsubscribe topic
     * @param topic topic name
     */
    void unsubscribe(String topic);

    /**
     * multi unsubscribe the topic
     * @param topics topic names
     */
    void unsubscribe(List<String> topics);
}
