package top.netkit.redis.client.command;



import java.util.List;

/**
 * list command executor
 * @author shixinke
 */
public interface ListCommandExecutor {
    /**
     * pop the element from the left of the list in block mode
     * @param key redis key
     * @param timeout execute timeout time
     * @param clazz response type
     * @return V
     */
    <V> V blPop(String key, long timeout, Class<V> clazz);

    /**
     * pop multi elements from the left of the list in block mode
     * @param keys redis keys
     * @param timeoutMills execute timeout time
     * @param clazz element value type
     * @return List
     */
    <V> List<V> blPop(List<String> keys, long timeoutMills, Class<V> clazz);

    /**
     * pop the element from the right of the list in block mode
     * @param key redis key
     * @param timeout execute timeout time
     * @param clazz element value type
     * @return V
     */
    <V> V brPop(String key, long timeout, Class<V> clazz);

    /**
     * pop multi elements from the right of the list in block mode
     * @param keys redis keys
     * @param timeout
     * @param clazz
     * @return
     */
    <V> List<V> brPop(List<String> keys, long timeout, Class<V> clazz);

    /**
     * pop an element to another list
     * @param sourceKey source key
     * @param destKey destination key
     * @param clazz element type
     * @return V
     */
    <V> V brPopLPush(String sourceKey, String destKey, Class<V> clazz);

    /**
     * get the element by index
     * @param key redis key
     * @param index index of the list
     * @param <V> element type
     * @param clazz element type
     * @return V
     */
    <V> V lIndex(String key, int index, Class<V> clazz);

    /**
     * insert into the list before the value
     * @param key redis key
     * @param pivot element
     * @param values insert values
     * @return boolean
     */
    <V> boolean lInsertBefore(String key, V pivot, List<V> values);

    /**
     * insert into the list after the value
     * @param key redis key
     * @param pivot element
     * @param values insert values
     * @return boolean
     */
    <V> boolean lInsertAfter(String key, V pivot, List<V> values);

    /**
     * get the length of the list
     * @param key redis key
     * @return int
     */
    int lLen(String key);

    /**
     * pop the value to left of the left
     * @param key redis key
     * @param clazz value type
     * @return V
     */
    <V> V lPop(String key, Class<V> clazz);

    /**
     * push value to the left of the list
     * @param key redis key
     * @param value element value
     * @return boolean
     */
    <V> boolean lPush(String key, V value);

    /**
     * multi push values
     * @param key redis key
     * @param values element values
     * @return boolean
     */
    <V> boolean lPush(String key, List<V> values);

    /**
     * push the value to left of the list if key is exist
     * @param key redis key
     * @param value element value
     * @return boolean
     */
    <V> boolean lPushX(String key, V value);

    /**
     * get elements from the range
     * @param key redis key
     * @param start index start
     * @param end index end
     * @param <V> element value
     * @param clazz class of element value
     * @return List
     */
    <V> List<V> lRange(String key, int start, int end, Class<V> clazz);

    /**
     * delete an element of the value
     * @param key redis key
     * @param value element value
     * @param count limit count
     * @return boolean
     */
    <V> boolean lRem(String key, V value, int count);

    /**
     * set the value of the index
     * @param key redis key
     * @param index list index
     * @param value element value
     * @return boolean
     */
    <V> boolean lSet(String key, int index, V value);

    /**
     * trim the list
     * @param key redis key
     * @param start start index
     * @param end end index
     * @return boolean
     */
    boolean lTrim(String key, int start, int end);

    /**
     * pop an element from right of the list
     * @param key redis key
     * @param clazz class of element value
     * @return V
     */
    <V> V rPop(String key, Class<V> clazz);

    /**
     * pop a value from the right of list and push it to the left of another list
     * @param sourceKey source key
     * @param destKey destination key
     * @param clazz class of the element
     * @return V
     */
    <V> V rPopLPush(String sourceKey, String destKey, Class<V> clazz);

    /**
     * push the value to the list
     * @param key redis key
     * @param value element value
     * @return boolean
     */
    <V> boolean rPush(String key, V value);

    /**
     * push the value if the list is exist
     * @param key redis key
     * @param value redis element value
     * @return V
     */
    <V> boolean rPushX(String key, V value);
}
