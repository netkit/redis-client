package top.netkit.redis.client.executor;

/**
 * redis lock exception
 * @author shixinke
 */
public class RedisConfigException extends RuntimeException {

    /**
     * redis config exception constructor
     */
    public RedisConfigException() {
        super();
    }

    /**
     * redis config exception constructor
     * @param message exception message
     */
    public RedisConfigException(String message) {
        super(message);
    }

    /**
     * redis config exception constructor
     * @param message exception message
     * @param cause exception
     */
    public RedisConfigException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * redis config exception constructor
     * @param cause cause
     */
    public RedisConfigException(Throwable cause) {
        super(cause);
    }

    /**
     * redis config exception constructor
     * @param message message
     * @param cause cause
     * @param enableSuppression enableSuppression
     * @param writableStackTrace writableStackTrace
     */
    protected RedisConfigException(String message, Throwable cause,
                                   boolean enableSuppression,
                                   boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
