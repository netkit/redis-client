package top.netkit.redis.client.executor;

import com.fasterxml.jackson.core.type.TypeReference;
import org.redisson.RedissonMultiLock;
import org.redisson.api.*;
import org.redisson.api.listener.MessageListener;
import org.redisson.api.listener.PatternMessageListener;
import org.redisson.client.codec.Codec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * redis client
 *
 * @author shixinke
 */
public class RedisClient implements RedisCommandExecutor, RedisLockExecutor {

    private static final Logger logger = LoggerFactory.getLogger(RedisClient.class);

    /**
     * redis command client
     */
    private final RedisCommandClient redisCommandClient;

    /**
     * redis lock client
     */
    private final RedisLockClient redisLockClient;

    /**
     * 编码
     */
    private Codec codec;



    public RedisClient(RedisCommandClient redisCommandClient, RedisLockClient redisLockClient) {
        this.redisCommandClient = redisCommandClient;
        this.redisLockClient = redisLockClient;
    }


    @Override
    public void setCodec(Codec codec) {
        this.codec = codec;
    }

    @Override
    public Codec getCodec() {
        return codec;
    }



    @Override
    public boolean hDel(String key, List<Object> fields) {
        return redisCommandClient.hDel(key, fields);
    }

    @Override
    public boolean hDel(String key, Object field) {
        return redisCommandClient.hDel(key, field);
    }

    @Override
    public boolean hExists(String key, Object filed) {
        return redisCommandClient.hExists(key, filed);
    }

    @Override
    public <V> V hGet(String key, Object field, Class<V> clazz) {
        return redisCommandClient.hGet(key, field, clazz);
    }

    @Override
    public <V> V hGet(String key, Object field, TypeReference<V> typeReference) {
        return redisCommandClient.hGet(key, field, typeReference);
    }



    /**
     * get all values of the key
     * @param key hash key
     * @param <V> hash member value type
     * @param clazz hash member value type
     * @return Map
     */
    public <V> Map<Object, V> hGetAll(String key, Class<V> clazz) {
        return redisCommandClient.hGetAll(key, clazz);
    }

    /**
     * get all values of the key
     * @param key hash key
     * @param keyClass key class
     * @param valueClass value class
     * @return Map
     */
    @Override
    public <K, V> Map<K, V> hGetAll(String key, Class<K> keyClass, Class<V> valueClass) {
        return redisCommandClient.hGetAll(key, keyClass, valueClass);
    }

    @Override
    public long hIncr(String key, Object field, int step) {
        return redisCommandClient.hIncr(key, field, step);
    }

    @Override
    public double hIncrByFloat(String key, Object field, double step) {
        return redisCommandClient.hIncrByFloat(key, field, step);
    }

    @Override
    public List<Object> hKeys(String key) {
        return redisCommandClient.hKeys(key);
    }

    @Override
    public long hLen(String key) {
        return redisCommandClient.hLen(key);
    }

    @Override
    public <K, V> Map<K, V> hMGet(String key, List<K> fields, Class<K> keyClass, Class<V> valueClass) {
        return redisCommandClient.hMGet(key, fields, keyClass, valueClass);
    }

    @Override
    public <K, V> boolean hMSet(String key, Map<K, V> valuesMap) {
        return redisCommandClient.hMSet(key, valuesMap);
    }

    @Override
    public <K, V> boolean hMSet(String key, Map<K, V> valuesMap, long expire) {
        return redisCommandClient.hMSet(key, valuesMap, expire);
    }


    @Override
    public <V> boolean hSet(String key, Object field, V value) {
        return redisCommandClient.hSet(key, field, value);
    }

    @Override
    public <V> boolean hSetNx(String key, Object field, V value) {
        return redisCommandClient.hSetNx(key, field, value);
    }

    @Override
    public long hStrLen(String key, Object field) {
        return redisCommandClient.hStrLen(key, field);
    }

    @Override
    public <V> List<V> hVals(String key, Class<V> clazz) {
        return redisCommandClient.hVals(key, clazz);
    }

    @Override
    public boolean del(String key) {
        return redisCommandClient.del(key);
    }

    @Override
    public boolean del(Collection<String> keys) {
        return redisCommandClient.del(keys);
    }

    @Override
    public boolean exists(String key) {
        return redisCommandClient.exists(key);
    }

    @Override
    public boolean expire(String key, long seconds) {
        try {
            return redisCommandClient.expire(key, seconds);
        } catch(Exception ex) {
            logger.error("expire failed, key={}", key, ex);
            return false;
        }
    }

    @Override
    public boolean expireAt(String key, int timestamp) {
        return redisCommandClient.expireAt(key, timestamp);
    }

    @Override
    public boolean pExpire(String key, long mills) {
        return redisCommandClient.pExpire(key, mills);
    }

    @Override
    public boolean pExpireAt(String key, long timestampMills) {
        return redisCommandClient.pExpireAt(key, timestampMills);
    }

    @Override
    public long ttl(String key) {
        return redisCommandClient.ttl(key);
    }

    @Override
    public long pTtl(String key) {
        return redisCommandClient.pTtl(key);
    }

    @Override
    public boolean persist(String key) {
        return redisCommandClient.persist(key);
    }

    @Override
    public boolean rename(String originalKey, String newKey) {
        return redisCommandClient.rename(originalKey, newKey);
    }

    @Override
    public boolean renameNx(String originalKey, String newKey) {
        return redisCommandClient.renameNx(originalKey, newKey);
    }

    @Override
    public String randomKey() {
        return redisCommandClient.randomKey();
    }

    @Override
    public String type(String key) {
        return redisCommandClient.type(key);
    }

    @Override
    public Iterable<String> keys(String pattern) {
        return redisCommandClient.keys(pattern);
    }

    @Override
    public <V> V blPop(String key, long timeout, Class<V> clazz) {
        return redisCommandClient.blPop(key, timeout, clazz);
    }

    @Override
    public <V> List<V> blPop(List<String> keys, long timeoutMills, Class<V> clazz) {
        return redisCommandClient.blPop(keys, timeoutMills, clazz);
    }

    @Override
    public <V> V brPop(String key, long timeout, Class<V> clazz) {
        return redisCommandClient.brPop(key, timeout, clazz);
    }

    @Override
    public <V> List<V> brPop(List<String> keys, long timeout, Class<V> clazz) {
        return redisCommandClient.brPop(keys, timeout, clazz);
    }

    @Override
    public <V> V brPopLPush(String sourceKey, String destKey, Class<V> clazz) {
        return redisCommandClient.brPopLPush(sourceKey, destKey, clazz);
    }

    @Override
    public <V> V lIndex(String key, int index, Class<V> clazz) {
        return redisCommandClient.lIndex(key, index, clazz);
    }


    @Override
    public <V> boolean lInsertBefore(String key, V pivot, List<V> values) {
        return redisCommandClient.lInsertBefore(key, pivot, values);
    }

    @Override
    public <V> boolean lInsertAfter(String key, V pivot, List<V> values) {
        return redisCommandClient.lInsertAfter(key, pivot, values);
    }


    @Override
    public int lLen(String key) {
        return redisCommandClient.lLen(key);
    }

    @Override
    public <V> V lPop(String key, Class<V> clazz) {
        return redisCommandClient.lPop(key, clazz);
    }

    @Override
    public <V> boolean lPush(String key, List<V> values) {
        return redisCommandClient.lPush(key, values);
    }


    @Override
    public <V> boolean lPush(String key, V value) {
        return redisCommandClient.lPush(key, value);
    }

    @Override
    public <V> boolean lPushX(String key, V value) {
        return redisCommandClient.lPushX(key, value);
    }

    @Override
    public <V> List<V> lRange(String key, int start, int end, Class<V> clazz) {
        return redisCommandClient.lRange(key, start, end, clazz);
    }

    @Override
    public <V> boolean lRem(String key, V value, int count) {
        return redisCommandClient.lRem(key, value, count);
    }

    @Override
    public <V> boolean lSet(String key, int index, V value) {
        return redisCommandClient.lSet(key, index, value);
    }

    @Override
    public boolean lTrim(String key, int start, int end) {
        return redisCommandClient.lTrim(key, start, end);
    }

    @Override
    public <V> V rPop(String key, Class<V> clazz) {
        return redisCommandClient.rPop(key, clazz);
    }

    @Override
    public <V> V rPopLPush(String sourceKey, String destKey, Class<V> clazz) {
        return redisCommandClient.rPopLPush(sourceKey, destKey, clazz);
    }

    @Override
    public <V> boolean rPush(String key, V value) {
        return redisCommandClient.rPush(key, value);
    }

    @Override
    public <V> boolean rPushX(String key, V value) {
        return redisCommandClient.rPushX(key, value);
    }

    @Override
    public <T> void pSubscribe(String pattern, Class<T> type, PatternMessageListener<T> listener) {
        redisCommandClient.pSubscribe(pattern, type, listener);
    }

    @Override
    public <T> void pSubscribe(List<String> patterns, List<Class<T>> types, List<PatternMessageListener<T>> patternMessageListeners) {
        redisCommandClient.pSubscribe(patterns, types, patternMessageListeners);
    }

    @Override
    public void pUnSubscribe(String pattern) {
        redisCommandClient.pUnSubscribe(pattern);
    }

    @Override
    public void pUnSubscribe(List<String> patterns) {
        redisCommandClient.pUnSubscribe(patterns);
    }

    @Override
    public <V> long publish(String channel, V message) {
        return redisCommandClient.publish(channel, message);
    }

    @Override
    public <M> void subscribe(String topic, Class<M> type, MessageListener<? extends M> listener) {
        redisCommandClient.subscribe(topic, type, listener);
    }

    @Override
    public <M> void subscribe(List<String> topics, List<Class<M>> clazz, List<MessageListener<? extends M>> messageListeners) {
        redisCommandClient.subscribe(topics, clazz, messageListeners);
    }

    @Override
    public void unsubscribe(String topic) {
        redisCommandClient.unsubscribe(topic);
    }

    @Override
    public void unsubscribe(List<String> topics) {
        redisCommandClient.unsubscribe(topics);
    }


    @Override
    public <V> V eval(String key, String script, boolean readonly, List<Object> keys, List<Object> args, Class<V> clazz) {
        return redisCommandClient.eval(key, script, readonly, keys, args, clazz);
    }

    @Override
    public <V> V eval(String script, boolean readonly, List<Object> keys, List<Object> args, Class<V> clazz) {
        return redisCommandClient.eval(script, readonly, keys, args, clazz);
    }

    @Override
    public <V> V eval(String script, boolean readonly, Class<V> clazz) {
        return redisCommandClient.eval(script, readonly,  clazz);
    }

    @Override
    public <V> V evalSha(String key, String script, boolean readonly, List<Object> keys, List<Object> args, Class<V> clazz) {
        return redisCommandClient.evalSha(key, script, readonly, keys, args, clazz);
    }

    @Override
    public <V> V evalSha(String script, boolean readonly, List<Object> keys, List<Object> args, Class<V> clazz) {
        return redisCommandClient.evalSha(script, readonly, keys, args, clazz);
    }

    @Override
    public <V> V evalSha(String script, boolean readonly, Class<V> clazz) {
        return redisCommandClient.evalSha(script, readonly, clazz);
    }


    @Override
    public boolean scriptExists(String script) {
        return redisCommandClient.scriptExists(script);
    }

    @Override
    public List<Boolean> scriptExists(List<String> scripts) {
        return redisCommandClient.scriptExists(scripts);
    }

    @Override
    public boolean flushScript() {
        return redisCommandClient.flushScript();
    }

    @Override
    public boolean killScript() {
        return redisCommandClient.killScript();
    }

    @Override
    public <V> V loadScript(String script) {
        return redisCommandClient.loadScript(script);
    }

    @Override
    public <V> boolean sAdd(String key, V value) {
        return redisCommandClient.sAdd(key, value);
    }

    @Override
    public <V> boolean sAdd(String key, List<V> values) {
        return redisCommandClient.sAdd(key, values);
    }

    @Override
    public int sCard(String key) {
        return redisCommandClient.sCard(key);
    }

    @Override
    public <V> Set<V> sDiff(String sourceKey, String destKey, Class<V> clazz) {
        return redisCommandClient.sDiff(sourceKey, destKey, clazz);
    }

    @Override
    public <V> Set<V> sDiff(String sourceKey, List<String> destKeys, Class<V> clazz) {
        return redisCommandClient.sDiff(sourceKey, destKeys, clazz);
    }

    @Override
    public <V> boolean sDiffStore(String storeKey, String sourceKey, List<String> destKeys, Class<V> clazz) {
        return redisCommandClient.sDiffStore(storeKey, sourceKey, destKeys, clazz);
    }

    @Override
    public <V> Set<V> sInter(String sourceKey, String destKey, Class<V> clazz) {
        return redisCommandClient.sInter(sourceKey, destKey, clazz);
    }

    @Override
    public <V> Set<V> sInter(String sourceKey, List<String> destKeys, Class<V> clazz) {
        return redisCommandClient.sInter(sourceKey, destKeys, clazz);
    }

    @Override
    public <V> boolean sInterStore(String storeKey, String sourceKey, List<String> destKeys, Class<V> clazz) {
        return redisCommandClient.sInterStore(storeKey, sourceKey, destKeys, clazz);
    }

    @Override
    public <V> Set<V> sUnion(String sourceKey, String destKey, Class<V> clazz) {
        return redisCommandClient.sUnion(sourceKey, destKey, clazz);
    }

    @Override
    public <V> Set<V> sUnion(String sourceKey, List<String> destKeys, Class<V> clazz) {
        return redisCommandClient.sUnion(sourceKey, destKeys, clazz);
    }

    @Override
    public <V> boolean sUnionStore(String storeKey, String sourceKey, List<String> destKeys, Class<V> clazz) {
        return redisCommandClient.sUnionStore(storeKey, sourceKey, destKeys, clazz);
    }

    @Override
    public <V> boolean sIsMember(String key, V member) {
        return redisCommandClient.sIsMember(key, member);
    }

    @Override
    public <V> Set<V> sMembers(String key, Class<V> clazz) {
        return redisCommandClient.sMembers(key, clazz);
    }

    @Override
    public <V> boolean sMove(String sourceKey, String destKey, V value) {
        return redisCommandClient.sMove(sourceKey, destKey, value);
    }

    @Override
    public <V> Set<V> sPop(String key, int count, Class<V> clazz) {
        return redisCommandClient.sPop(key, count, clazz);
    }

    @Override
    public <V> boolean sRem(String key, List<V> values) {
        return redisCommandClient.sRem(key, values);
    }


    @Override
    public <V> boolean sRem(String key, V value) {
        return redisCommandClient.sRem(key, value);
    }


    @Override
    public <V> Set<V> sRandomMember(String key, int count, Class<V> clazz) {
        return redisCommandClient.sRandomMember(key, count, clazz);
    }


    @Override
    public <V> boolean zAdd(String key, V member, Double score) {
        return redisCommandClient.zAdd(key, member, score);
    }

    @Override
    public <V> boolean zAdd(String key, Map<V, Double> members) {
        return redisCommandClient.zAdd(key, members);
    }

    @Override
    public int zCard(String key) {
        return redisCommandClient.zCard(key);
    }

    @Override
    public int zCount(String key, double min, double max) {
        return redisCommandClient.zCount(key, min, max);
    }

    @Override
    public <V> double zIncrBy(String key, V member, double step) {
        return redisCommandClient.zIncrBy(key, member, step);
    }



    @Override
    public boolean zInterStore(String storeKey, String sourceKey, List<String> destKeys) {
        return redisCommandClient.zInterStore(storeKey, sourceKey, destKeys);
    }

    @Override
    public long zLexCount(String key, String minMember, String maxMember) {
        return redisCommandClient.zLexCount(key, minMember, maxMember);
    }

    @Override
    public <V> List<V> zRangeWithoutScore(String key, int start, int end, Class<V> clazz) {
        return redisCommandClient.zRangeWithoutScore(key, start, end, clazz);
    }

    @Override
    public <V> List<V> zRevRangeWithoutScore(String key, int start, int end, Class<V> clazz) {
        return redisCommandClient.zRevRangeWithoutScore(key, start, end, clazz);
    }

    @Override
    public <V> int zRank(String key, V member) {
        return redisCommandClient.zRank(key, member);
    }

    @Override
    public <V> int zRevRank(String key, V member) {
        return redisCommandClient.zRevRank(key, member);
    }

    @Override
    public <V> boolean zRem(String key, V member) {
        return redisCommandClient.zRem(key, member);
    }

    @Override
    public <V> boolean zRem(String key, List<V> members) {
        return redisCommandClient.zRem(key, members);
    }


    @Override
    public <V> List<Map<V, Double>> zRange(String key, int start, int end, Class<V> clazz) {
        return redisCommandClient.zRange(key, start, end, clazz);
    }

    @Override
    public <V> Map<V, Double> zRangeByScore(String key, int start, int end, boolean withScores, long offset, long count, Class<V> clazz) {
        return redisCommandClient.zRangeByScore(key, start, end, withScores, offset, count, clazz);
    }

    @Override
    public <V> List<Map<V, Double>> zRevRange(String key, int start, int end, Class<V> clazz) {
        return redisCommandClient.zRevRange(key, start, end, clazz);
    }

    @Override
    public <V> Map<V, Double> zRevRangeByScore(String key, int start, int end, boolean withScores, long offset, long count, Class<V> clazz) {
        return redisCommandClient.zRevRangeByScore(key, start, end, withScores, offset, count, clazz);
    }


    @Override
    public boolean zRemRangeByRank(String key, int start, int end) {
        return redisCommandClient.zRemRangeByRank(key, start, end);
    }

    @Override
    public boolean zRemRangeByScope(String key, double min, double max) {
        return redisCommandClient.zRemRangeByScope(key, min, max);
    }

    @Override
    public <V> Double zScore(String key, V member) {
        return redisCommandClient.zScore(key, member);
    }


    @Override
    public boolean zUnionStore(String storeKey, String sourceKey, List<String> destKeys) {
        return redisCommandClient.zUnionStore(storeKey, sourceKey, destKeys);
    }

    @Override
    public boolean append(String key, String value) {
        return redisCommandClient.append(key, value);
    }

    @Override
    public long bitCount(String key) {
        return redisCommandClient.bitCount(key);
    }

    @Override
    public void bitOp(String destKey, String operation, String... keys) {
        redisCommandClient.bitOp(destKey, operation, keys);
    }

    @Override
    public void and(String destKey, String... keys) {
        redisCommandClient.and(destKey, keys);
    }

    @Override
    public void or(String destKey, String... keys) {
        redisCommandClient.or(destKey, keys);
    }

    @Override
    public void not(String destKey) {
        redisCommandClient.not(destKey);
    }

    @Override
    public void xor(String destKey, String... keys) {
        redisCommandClient.xor(destKey, keys);
    }


    @Override
    public <V> boolean set(String key, V value) {
        return redisCommandClient.set(key, value);
    }

    @Override
    public <V> boolean set(String key, V value, long expireSeconds) {
        return redisCommandClient.set(key, value, expireSeconds);
    }

    @Override
    public <V> boolean setEx(String key, V value, long expireSeconds) {
        return redisCommandClient.setEx(key, value, expireSeconds);
    }

    @Override
    public <V> boolean pSetEx(String key, V value, long expireMills) {
        return redisCommandClient.pSetEx(key, value, expireMills);
    }

    @Override
    public <V> boolean setNx(String key, V value) {
        return redisCommandClient.setNx(key, value);
    }

    @Override
    public String get(String key) {
        try {
            return redisCommandClient.get(key);
        } catch (Exception ex) {
            logger.error("get failed, key={}", key, ex);
            return null;
        }

    }

    @Override
    public <V> V get(String key, Class<V> clazz) {
        try {
            return redisCommandClient.get(key, clazz);
        } catch (Exception ex) {
            logger.error("get failed, key={}", key, ex);
            return null;
        }

    }

    @Override
    public <V> V get(String key, TypeReference<V> typeReference) {
        try {
            return redisCommandClient.get(key, typeReference);
        } catch (Exception ex) {
            logger.error("get failed, key={}", key, ex);
            return null;
        }
    }

    @Override
    public <V> V getSet(String key, V value, Class<V> clazz) {
        return redisCommandClient.getSet(key, value, clazz);
    }



    @Override
    public long incr(String key) {
        return redisCommandClient.incr(key);
    }

    @Override
    public long incrBy(String key, int step) {
        return redisCommandClient.incrBy(key, step);
    }

    @Override
    public double incrByFloat(String key, float step) {
        return redisCommandClient.incrByFloat(key, step);
    }

    @Override
    public long decr(String key) {
        return redisCommandClient.decr(key);
    }

    @Override
    public long decrBy(String key, int step) {
        return redisCommandClient.decrBy(key, step);
    }

    @Override
    public <V> Map<String, V> mGet(List<String> keys, Class<V> clazz) {
        return redisCommandClient.mGet(keys, clazz);
    }

    @Override
    public <V> Map<String, V> mGet(List<String> keys, TypeReference<V> typeReference) {
        return redisCommandClient.mGet(keys, typeReference);
    }

    @Override
    public <V> boolean mSet(Map<String, V> valueMap) {
        return redisCommandClient.mSet(valueMap);
    }

    @Override
    public <V> boolean mSetEx(Map<String, V> valueMap, long expire) {
        return redisCommandClient.mSetEx(valueMap, expire);
    }

    @Override
    public <V> boolean mSetNx(Map<String, V> valueMap) {
        return redisCommandClient.mSetNx(valueMap);
    }


    @Override
    public boolean setRange(String key, int offset, String value) {
        return redisCommandClient.setRange(key, offset, value);
    }

    @Override
    public long strLen(String key) {
        return redisCommandClient.strLen(key);
    }

    @Override
    public boolean discard(RTransaction transaction) {
        return redisCommandClient.discard(transaction);
    }


    @Override
    public boolean multi(ReturnableExecutor<RBatch, Boolean> executor) {
        return redisCommandClient.multi(executor);
    }

    @Override
    public boolean pipeline(ReturnableExecutor<RBatch, Boolean> executor) {
        return redisCommandClient.pipeline(executor);
    }

    @Override
    public <T> T lock(String key, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor) {
        return redisLockClient.lock(key, leaseTime, timeUnit, executor);
    }

    @Override
    public void lock(String key, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor) {
        redisLockClient.lock(key, leaseTime, timeUnit, executor);
    }

    @Override
    public <T> T lock(String key, boolean fair, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor) {
        return redisLockClient.lock(key, fair, leaseTime, timeUnit, executor);
    }

    @Override
    public void lock(String key, boolean fair, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor) {
        redisLockClient.lock(key, fair, leaseTime, timeUnit, executor);
    }

    @Override
    public <T> T lock(String key, long leaseTime, Function<RLock, T> executor) {
        return redisLockClient.lock(key, leaseTime, executor);
    }

    @Override
    public void lock(String key, long leaseTime, Consumer<RLock> executor) {
        redisLockClient.lock(key, leaseTime, executor);
    }

    @Override
    public <T> T tryLock(String key, long waitTime, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor) {
        return redisLockClient.tryLock(key, waitTime, leaseTime, timeUnit, executor);
    }

    @Override
    public void tryLock(String key, long waitTime, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor) {
        redisLockClient.tryLock(key, waitTime, leaseTime, timeUnit, executor);
    }

    @Override
    public <T> T tryLock(String key, boolean fair, long waitTime, long leaseTime, TimeUnit timeUnit, Function<RLock, T> executor) {
        return redisLockClient.tryLock(key, fair, waitTime, leaseTime, timeUnit, executor);
    }

    @Override
    public void tryLock(String key, boolean fair, long waitTime, long leaseTime, TimeUnit timeUnit, Consumer<RLock> executor) {
        redisLockClient.tryLock(key, fair, waitTime, leaseTime, timeUnit, executor);
    }

    @Override
    public <T> T tryLock(String key, long waitTime, long leaseTime, Function<RLock, T> executor) {
        return redisLockClient.tryLock(key, waitTime, leaseTime, executor);
    }

    @Override
    public void tryLock(String key, long waitTime, long leaseTime, Consumer<RLock> executor) {
        redisLockClient.tryLock(key, waitTime, leaseTime, executor);
    }

    @Override
    public <R, T> T readWriteLock(String key, long readWaitTime, long readLeaseTime, long writeWaitTime, long writeLeaseTime, Function<ReadWriteLock, R> readExecutor, BiFunction<ReadWriteLock, R, T> writeExecutor) {
        return redisLockClient.readWriteLock(key, readWaitTime, readLeaseTime, writeWaitTime, writeLeaseTime, readExecutor, writeExecutor);
    }

    @Override
    public <T> T multiLock(List<String> keys, long leaseTime, TimeUnit timeUnit, Function<RedissonMultiLock, T> executor) {
        return redisLockClient.multiLock(keys, leaseTime, timeUnit, executor);
    }

    @Override
    public <T> T multiLock(List<String> keys, long leaseTime, Function<RedissonMultiLock, T> executor) {
        return redisLockClient.multiLock(keys, leaseTime, executor);
    }

    @Override
    public void multiLock(List<String> keys, long leaseTime, TimeUnit timeUnit, Consumer<RedissonMultiLock> executor) {
        redisLockClient.multiLock(keys, leaseTime, timeUnit, executor);
    }

    @Override
    public void multiLock(List<String> keys, long leaseTime, Consumer<RedissonMultiLock> executor) {
        redisLockClient.multiLock(keys, leaseTime, executor);
    }

    @Override
    public <T> T tryMultiLock(List<String> keys, long waitTime, long leaseTime, TimeUnit timeUnit, Function<RedissonMultiLock, T> executor) {
        return redisLockClient.tryMultiLock(keys, waitTime, leaseTime, timeUnit, executor);
    }

    @Override
    public <T> T tryMultiLock(List<String> keys, long waitTime, long leaseTime, Function<RedissonMultiLock, T> executor) {
        return redisLockClient.tryMultiLock(keys, waitTime, leaseTime, executor);
    }

    @Override
    public void tryMultiLock(List<String> keys, long waitTime, long leaseTime, TimeUnit timeUnit, Consumer<RedissonMultiLock> executor) {
        redisLockClient.tryMultiLock(keys, waitTime, leaseTime, timeUnit, executor);
    }

    @Override
    public void tryMultiLock(List<String> keys, long waitTime, long leaseTime, Consumer<RedissonMultiLock> executor) {
        return redisLockClient.tryMultiLock(keys, waitTime, leaseTime, executor);
    }
}
