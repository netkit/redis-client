package top.netkit.redis.client.executor;

/**
 * executor without return value
 * @author shixinke
 */
public interface VoidExecutor {

    /**
     * execution
     */
    void execute();
}
