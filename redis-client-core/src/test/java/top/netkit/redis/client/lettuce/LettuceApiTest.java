package top.netkit.redis.client.lettuce;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.sync.RedisCommands;

public class LettuceApiTest {



    public static <K,V> void set(RedisCommands<K, V> redisCommands, K key, V value) {
        System.out.println(redisCommands.set(key, value));
    }

    public static <K,V> void get(RedisCommands<K, V> redisCommands, K key) {
        System.out.println(redisCommands.get(key));
    }


}
