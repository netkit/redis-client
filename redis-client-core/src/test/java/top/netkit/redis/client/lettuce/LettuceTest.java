package top.netkit.redis.client.lettuce;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.sync.RedisCommands;
import io.lettuce.core.protocol.RedisCommand;

import java.nio.charset.StandardCharsets;

public class LettuceTest {

    public static void main(String[] args) {
        keyTest();
    }

    private static void keyTest() {
        RedisClient client = getClient();
        RedisCommands<String, String> redisCommand = client.connect().sync();
        LettuceApiTest.set(redisCommand, "lettuce:key:test", "value");
        LettuceApiTest.get(redisCommand, "lettuce:key:test");

    }


    private static RedisClient getClient() {
        return RedisClient.create(RedisURI.Builder.redis("192.168.199.126", 6379).withPassword("Sh5VvroY4gR1nCND").withDatabase(0).build());
    }
}
