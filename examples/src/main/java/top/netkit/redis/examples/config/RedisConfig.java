package top.netkit.redis.examples.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.netkit.redis.client.executor.RedisClient;
import top.netkit.redis.client.executor.RedisCommandClient;
import top.netkit.redis.client.executor.RedisLockClient;

/**
 * redis config
 * @author shixinke
 */
@Configuration
public class RedisConfig {

    @Value("${redis.host}")
    private String host;

    @Value("${redis.port}")
    private int port;

    @Value("${redis.password}")
    private String password;

    @Value("${redis.database}")
    private int database;

    @Bean
    public RedisClient getClient() {
        return new RedisClient(getCommandClient(), getLockClient());
    }

    @Bean
    public RedisCommandClient getCommandClient() {
        return new RedisCommandClient(getRedissonClient());
    }

    @Bean
    public RedisLockClient getLockClient() {
        return new RedisLockClient(getRedissonClient());
    }

    @Bean
    public  RedissonClient getRedissonClient() {
        Config config = new Config();
        config.useSingleServer()
                .setTimeout(1000000)
                .setAddress("redis://"+host+":"+port+"").setPassword(password).setDatabase(database);
        return Redisson.create(config);
    }


}
