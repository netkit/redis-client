package top.netkit.redis.examples.util;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.client.codec.StringCodec;
import org.redisson.config.Config;
import top.netkit.redis.client.executor.RedisClient;
import top.netkit.redis.client.executor.RedisCommandClient;
import top.netkit.redis.client.executor.RedisLockClient;

/**
 * redis client util
 * @author shixinke
 */
public class RedisClientUtil {

    public static RedisClient getClient() {
        return new RedisClient(getCommandClient(), getLockClient());
    }

    public static RedisCommandClient getCommandClient() {
        return new RedisCommandClient(getRedissonClient());
    }

    public static RedisLockClient getLockClient() {
        return new RedisLockClient(getRedissonClient());
    }

    public static RedissonClient getRedissonClient() {
        Config config = new Config();
        config.useSingleServer()
                .setTimeout(1000000)
                .setAddress("redis://127.0.0.1:6379").setPassword("redispassword").setDatabase(13);
        return Redisson.create(config);
    }
}
