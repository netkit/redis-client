package top.netkit.redis.examples.data;

import lombok.Data;

/**
 * 用户信息
 * @author shixinke
 */
@Data
public class User {

    private long id;

    private String name;

    private int age;

}
