package top.netkit.redis.examples.command;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import top.netkit.redis.client.command.KeyCommandExecutor;
import top.netkit.redis.client.executor.RedisClient;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collection;

@Slf4j
@SpringBootTest
public class KeyTest  {

    @Resource
    private RedisClient redisClient;

    @Test
    public void delTest() {
        String key = "test_key";
        boolean result = redisClient.setEx(key, "test_value", 100L);
        log.info("set.result={}, data={}", result, redisClient.get(key));
        log.info("delete.result={}", redisClient.del(key));
    }


    @Test
    public void delKeysTest() {
        String key1 = "test_delete1";
        String key2 = "test_delete2";
        redisClient.set(key1, "test1");
        redisClient.set(key2, "test2");
        log.info("delete={}", redisClient.del(Arrays.asList(key1, key2)));
    }


    @Test
    public void existsTest() {
        String key = "test_exists";
        log.info("result={}", redisClient.exists(key));
    }

    @Test
    public void expireTest() {
        String key = "test_expire";
        log.info("result={}", redisClient.expire(key, 3600));
    }


    @Test
    public void expireAtTest() {
        String key = "test_expireat";
        log.info("result={}", redisClient.expireAt(key, (int) (System.currentTimeMillis() / 1000) + 3600));
    }

    @Test
    public void pExpireTest() {
        String key = "test_pexpire";
        log.info("result={}", redisClient.pExpire(key, 3600));
    }

    @Test
    public void pExpireAtTest() {
        String key = "test_pexpireat";
        log.info("result={}", redisClient.pExpireAt(key, System.currentTimeMillis() + 3600 * 1000));
    }

    @Test
    public void ttlTest() {
        String key = "test_ttl";
        redisClient.setEx(key, "test_ttl", 3600);
        log.info("result={}", redisClient.ttl(key));
    }

    @Test
    public void pTtlTest() {
        String key = "test_pttl";
        redisClient.setEx(key, "test_pttl", 3600);
        log.info("result={}", redisClient.pTtl(key));
    }

    @Test
    public void persistTest() {
        log.info("{}", redisClient.persist("test_persist"));
    }

    @Test
    public void renameTest() {
        log.info("result={}", redisClient.rename("test_source", "test_target"));
    }

    @Test
    public void renameNxTest() {
        String key = "test_name";
        redisClient.setEx(key, "test_name", 3600);
        boolean result1 = redisClient.renameNx(key, "test_rename");
        log.info("result={}; result2={}", result1, redisClient.renameNx("test_any", "test_any_rename"));
    }

    @Test
    public void randomKeyTest() {
        log.info("key={}", redisClient.randomKey());
    }

    @Test
    public void typeTest() {
        String key1 = "test_type1";
        String key2 = "test_type2";
        String key3 = "test_type3";
        String key4 = "test_type4";
        String key5 = "test_type5";
        redisClient.setEx(key1, "test", 3600);
        redisClient.hSet(key2, "field1", "test2");
        redisClient.lPush(key3, "100");
        redisClient.sAdd(key4, "s1");
        redisClient.zAdd(key5, "z1", 10d);
        log.info("t1={}, t2={}, t3={}, t4={}, t5={}", redisClient.type(key1), redisClient.type(key2), redisClient.type(key3), redisClient.type(key4), redisClient.type(key5));
    }

    @Test
    public void keysTest() {
        log.info("keys={}", redisClient.keys("test*"));
    }
}
