package top.netkit.redis.examples.command;

import lombok.extern.slf4j.Slf4j;
import org.redisson.client.codec.StringCodec;
import top.netkit.redis.examples.data.User;
import top.netkit.redis.examples.util.RedisClientUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * hash 测试
 * @author shixinke
 */

@Slf4j
public class HashNativeTest {

    public static void main(String[] args) {
        //hSetNative();
        //hMSetNative();
        //hMsetTest();
        //hSetTest();
        //hSetBeanTest();
        hGetBeanTest();
        //hSetNative();
    }


    public static void hMsetTest() {
        Map<String, Object> dataMap = new HashMap<>(2);
        dataMap.put("id", "1");
        dataMap.put("name", "test");
        RedisClientUtil.getClient().hMSet("test:0:map", dataMap);
    }

    public static void hSetBeanTest() {
        User user = new User();
        user.setId(100L);
        user.setAge(35);
        user.setName("user456");
        RedisClientUtil.getClient().hSet("test:3:map", "user", user);
    }

    public static void hGetBeanTest() {
        log.info("{}", RedisClientUtil.getClient().hGet("test:3:map", "user", User.class));
    }

    public static void hSetTest() {
        RedisClientUtil.getClient().hSet("test:0:map", "age", 36);
    }

    public static void hSetNative() {
        Object field = "id";
        //是否允许单引号来包住属性名称和字符串值
        RedisClientUtil.getRedissonClient().getMap("test:2:map", new StringCodec()).put(field, 2);
    }



}
