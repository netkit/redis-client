package top.netkit.redis.starter.annotation;

/**
 * 缓存注解
 * @author shixinke
 */
public @interface Cacheable {
}
