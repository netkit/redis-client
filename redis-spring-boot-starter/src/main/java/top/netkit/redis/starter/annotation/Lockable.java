package top.netkit.redis.starter.annotation;

/**
 * 分布式锁注解
 * @author shixinke
 */
public @interface Lockable {
}
